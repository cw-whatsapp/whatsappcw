import 'package:flutter/material.dart';

showLoaderDialog(BuildContext context) {
  AlertDialog alert = AlertDialog(
    content: SizedBox(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // const SpinKitCircle(
          //   color: UtilsColors.secundary,
          //   size: 40,
          // ),
          Container(
              margin: const EdgeInsets.only(left: 7),
              child: const Text(
                " Procesando...",
                style:
                    TextStyle(fontWeight: FontWeight.w500, letterSpacing: 1.5),
              )),
        ],
      ),
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
