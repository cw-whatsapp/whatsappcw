// ignore_for_file: prefer_const_constructors

import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path/path.dart';
import 'package:whatsapp_cw/models/sponsor.dart';
import 'package:whatsapp_cw/screens/home.dart';
import 'package:whatsapp_cw/whatsapp/bloc/bloc_whatsapp/blocwhatsapp_bloc.dart';
import 'package:whatsapp_cw/whatsapp/model/model_envio_list.dart';
import 'package:whatsapp_cw/widgets/changelog.dart';
import 'package:whatsapp_cw/widgets/material_equivalents.dart';
import 'package:whatsapp_cw/widgets/page.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:whatsapp_cw/widgets/sponsor.dart';

import '../../widgets/card_highlight.dart';
import 'package:url_launcher/link.dart';

class ButtonPage extends StatefulWidget {
  const ButtonPage({Key? key}) : super(key: key);

  @override
  State<ButtonPage> createState() => _ButtonPageState();
}

class _ButtonPageState extends State<ButtonPage> with PageMixin {
  bool simpleDisabled = false;
  bool filledDisabled = false;
  bool iconDisabled = false;
  bool toggleDisabled = false;
  bool toggleState = false;
  bool splitButtonDisabled = false;
  bool radioButtonDisabled = false;
  int radioButtonSelected = -1;

  bool selected = true;
  String? comboboxValue;
  final TextEditingController _controllerNumero = TextEditingController();
  final TextEditingController _controllerNumero2 = TextEditingController();
  BlocwhatsappBloc _bloc = BlocwhatsappBloc();

  String? selectedCat;

  final expanderKey = GlobalKey<ExpanderState>(debugLabel: 'Expander key');
  final expanderKey2 = GlobalKey<ExpanderState>(debugLabel: 'Expander key2');
  ModelEnvioList? selectedObjectCat;
  @override
  void initState() {
    // TODO: implement initState
    _controllerNumero.text = "51";
    _controllerNumero2.text = "51";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasFluentTheme(context));
    final theme = FluentTheme.of(context);

    return BlocProvider.value(
      value: _bloc,
      child: BlocBuilder<BlocwhatsappBloc, BlocwhatsappState>(
        builder: (context, state) {
          return ScaffoldPage.scrollable(
            header: PageHeader(
              title: Text('Reportes '),
              commandBar:
                  Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                // Link(
                //   uri: Uri.parse('https://github.com/bdlukaa/fluent_ui'),
                //   builder: (context, open) => Tooltip(
                //     message: 'Source code',
                //     child: IconButton(
                //       icon: const Icon(FluentIcons.open_source, size: 24.0),
                //       onPressed: open,
                //     ),
                //   ),
                // ),
              ]),
            ),
            children: [
              Row(
                children: [
                  Expanded(
                    child: Container(
                      child: Column(
                        children: [
                          Expander(
                            key: expanderKey,
                            header: const Text('Reporte envios'),
                            content: Container(
                              height: 200,
                              width: double.infinity,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Especificar el numero de Whatsapp.',
                                  ),
                                  TextBox(
                                    controller: _controllerNumero,
                                    //header: 'NOMBRES:',
                                    placeholder: 'Número',
                                    expands: false,
                                    autofocus: true,
                                    // cursorColor: (_descriptionStatus) ? Colors.red : Colors.blue,
                                    highlightColor: null,
                                    prefix: Container(
                                      padding:
                                          const EdgeInsets.fromLTRB(5, 0, 5, 0),
                                      child: const Icon(FluentIcons.phone,
                                          size: 22.0),
                                    ),
                                    //highlightColor: Colors.green,
                                    unfocusedColor: Colors.transparent,
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    width: double.infinity,
                                    child: FilledButton(
                                      child: const Text('Generar reporte'),
                                      onPressed: () {
                                        if (_controllerNumero.text.length ==
                                            11) {
                                          _bloc.add(
                                            GetReportEnvio(
                                              context: context,
                                              number: _controllerNumero.text,
                                            ),
                                          );
                                        } else {
                                          displayInfoBar(context,
                                              builder: (context, close) {
                                            return InfoBar(
                                              title: const Text('Se requiere,'),
                                              content: const Text(
                                                  'el número de Whatsapp del cual quiere generar el reporte.'),
                                              action: IconButton(
                                                icon: const Icon(
                                                    FluentIcons.clear),
                                                onPressed: close,
                                              ),
                                              severity: InfoBarSeverity.warning,
                                            );
                                          });
                                        }
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            // onStateChanged: (open) => setState(() {}),
                            initiallyExpanded: true,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(width: 20),
                  Expanded(
                    child: Expander(
                      key: expanderKey2,
                      header: const Text('Reporte Inactivos '),
                      content: Container(
                        height: 200,
                        width: double.infinity,
                        child: (state.statusExelExistencia == true)
                            ? ContentDialog(
                                style: const ContentDialogThemeData(
                                    padding: EdgeInsets.all(30)),
                                constraints:
                                    const BoxConstraints(maxWidth: 600),
                                content: () {
                                  return Column(
                                    children: [
                                      const ProgressRing(),
                                      Text(
                                          "Revisando grupos ${state.countExitencias} de ${state.countGroupExistencias}")
                                    ],
                                  );
                                }(),
                              )
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Especificar el numero de Whatsapp.',
                                  ),
                                  TextBox(
                                    controller: _controllerNumero2,
                                    //header: 'NOMBRES:',
                                    placeholder: 'Número',
                                    expands: false,
                                    autofocus: true,
                                    // cursorColor: (_descriptionStatus) ? Colors.red : Colors.blue,
                                    highlightColor: null,
                                    prefix: Container(
                                      padding:
                                          const EdgeInsets.fromLTRB(5, 0, 5, 0),
                                      child: const Icon(FluentIcons.phone,
                                          size: 22.0),
                                    ),
                                    //highlightColor: Colors.green,
                                    unfocusedColor: Colors.transparent,
                                    onEditingComplete: () {
                                      log("------ onEditingComplete");
                                    },
                                    onChanged: (v) {
                                      log(v.length.toString());
                                      if (v.length == 11) {
                                        _bloc.add(GetEnvioList(number: v));
                                      }
                                    },
                                    onSubmitted: (value) {
                                      log("------ onSubmitted $value");
                                    },
                                    onTap: () {
                                      log("------ onTap");
                                    },
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  const Text(
                                    'Seleccionar Envio deseado.',
                                  ),
                                  Container(
                                    width: double.infinity,
                                    child: AutoSuggestBox<ModelEnvioList>(
                                      enabled: true,
                                      items: state.listEnvio!
                                          .map<
                                              AutoSuggestBoxItem<
                                                  ModelEnvioList>>(
                                            (cat) => AutoSuggestBoxItem<
                                                ModelEnvioList>(
                                              value: cat,
                                              label:
                                                  "${cat.envioDate} - ${cat.envioDescription!}",
                                              onFocusChange: (focused) {
                                                print(
                                                    'Focused ${cat.envioDate}');
                                                if (focused) {
                                                  print(
                                                      'Focused ${cat.envioDate}');
                                                }
                                              },
                                            ),
                                          )
                                          .toList(),
                                      onSelected: (item) {
                                        print(
                                            "-----> ${item.value!.envioDate!}");
                                        print(
                                            "-----> ${item.value!.envioDescription!}");
                                        setState(() =>
                                            selectedObjectCat = item.value);
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    width: double.infinity,
                                    child: FilledButton(
                                      child: const Text('Generar reporte'),
                                      onPressed: () {
                                        if (_controllerNumero2.text.length ==
                                                11 &&
                                            selectedObjectCat != null) {
                                          _bloc.add(
                                            GenerateExelExistencias(
                                              context: context,
                                              envioDate: selectedObjectCat!,
                                              number: _controllerNumero2.text,
                                            ),
                                          );
                                        } else {
                                          displayInfoBar(context,
                                              builder: (context, close) {
                                            return InfoBar(
                                              title: const Text('Se requiere,'),
                                              content: const Text(
                                                  'el número de Whatsapp y fecha son obligatorio para generar el reporte.'),
                                              action: IconButton(
                                                icon: const Icon(
                                                    FluentIcons.clear),
                                                onPressed: close,
                                              ),
                                              severity: InfoBarSeverity.warning,
                                            );
                                          });
                                        }
                                      },
                                    ),
                                  )
                                ],
                              ),
                      ),
                      // onStateChanged: (open) => setState(() {}),
                      initiallyExpanded: true,
                    ),
                  ),
                ],
              )
            ],
          );
        },
      ),
    );
  }
}

class NewWidget extends StatelessWidget {
  const NewWidget({
    Key? key,
    required BlocwhatsappBloc bloc,
    required TextEditingController controllerNumero,
  })  : _bloc = bloc,
        _controllerNumero = controllerNumero,
        super(key: key);

  final BlocwhatsappBloc _bloc;
  final TextEditingController _controllerNumero;

  @override
  Widget build(BuildContext context) {
    return ContentDialog(
      constraints: const BoxConstraints(maxWidth: 600),
      title: Row(
        children: [
          const Text("☎--->"),
          const SizedBox(width: 8.0),
          const Expanded(child: Text('')),
          SmallIconButton(
            child: Tooltip(
              message: FluentLocalizations.of(context).closeButtonLabel,
              child: IconButton(
                icon: const Icon(FluentIcons.chrome_close),
                onPressed: Navigator.of(context).pop,
              ),
            ),
          ),
        ],
      ),
      content: Container(
        height: 60,
        width: double.infinity,
        child: BlocBuilder<BlocwhatsappBloc, BlocwhatsappState>(
            builder: (context, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Especificar el numero de Whatsapp. ${state.envioSelect}',
              ),
              Expanded(
                child: ComboBox<String>(
                  isExpanded: false,
                  value: state.envioSelect,
                  items: cats.map<ComboBoxItem<String>>((e) {
                    return ComboBoxItem<String>(
                      child: Text(e),
                      value: e,
                    );
                  }).toList(),
                  onChanged: (color) {
                    _bloc.add(EnvioSelect(envioSelect: color));
                  },
                  placeholder: const Text('Select a cat breed'),
                ),
              ),
            ],
          );
        }),
      ),
      actions: [
        FilledButton(
          child: const Text('Generar reporte'),
          onPressed: () {
            if (_controllerNumero.text != "" ||
                _controllerNumero.text.length == 9) {
              _bloc.add(
                GetReportEnvio(
                  context: context,
                  number: _controllerNumero.text,
                ),
              );

              Navigator.pop(context, 'close');
            } else {
              displayInfoBar(context, builder: (context, close) {
                return InfoBar(
                  title: const Text('Se requiere,'),
                  content: const Text(
                      'el número de Whatsapp del cual quiere generar el reporte.'),
                  action: IconButton(
                    icon: const Icon(FluentIcons.clear),
                    onPressed: close,
                  ),
                  severity: InfoBarSeverity.warning,
                );
              });
            }
          },
        ),
      ],
    );
  }
}

const cats = <String>[
  'Abyssinian',
  'Aegean',
  'American Bobtail',
  'American Curl',
  'American Ringtail',
  'American Shorthair',
  'American Wirehair',
  'Aphrodite Giant',
  'Arabian Mau',
  'Asian cat',
  'Asian Semi-longhair',
  'Australian Mist',
  'Balinese',
  'Bambino',
  'Bengal',
  'Birman',
  'Bombay',
  'Brazilian Shorthair',
  'British Longhair',
  'British Shorthair',
  'Burmese',
  'Burmilla',
  'California Spangled',
  'Chantilly-Tiffany',
  'Chartreux',
  'Chausie',
  'Colorpoint Shorthair',
  'Cornish Rex',
  'Cymric',
  'Cyprus',
  'Devon Rex',
  'Donskoy',
  'Dragon Li',
  'Dwelf',
  'Egyptian Mau',
  'European Shorthair',
  'Exotic Shorthair',
  'Foldex',
  'German Rex',
  'Havana Brown',
  'Highlander',
  'Himalayan',
  'Japanese Bobtail',
  'Javanese',
  'Kanaani',
  'Khao Manee',
  'Kinkalow',
  'Korat',
  'Korean Bobtail',
  'Korn Ja',
  'Kurilian Bobtail',
  'Lambkin',
  'LaPerm',
  'Lykoi',
  'Maine Coon',
  'Manx',
  'Mekong Bobtail',
  'Minskin',
  'Napoleon',
  'Munchkin',
  'Nebelung',
  'Norwegian Forest Cat',
  'Ocicat',
  'Ojos Azules',
  'Oregon Rex',
  'Oriental Bicolor',
  'Oriental Longhair',
  'Oriental Shorthair',
  'Persian (modern)',
  'Persian (traditional)',
  'Peterbald',
  'Pixie-bob',
  'Ragamuffin',
  'Ragdoll',
  'Raas',
  'Russian Blue',
  'Russian White',
  'Sam Sawet',
  'Savannah',
  'Scottish Fold',
  'Selkirk Rex',
  'Serengeti',
  'Serrade Petit',
  'Siamese',
  'Siberian or´Siberian Forest Cat',
  'Singapura',
  'Snowshoe',
  'Sokoke',
  'Somali',
  'Sphynx',
  'Suphalak',
  'Thai',
  'Thai Lilac',
  'Tonkinese',
  'Toyger',
  'Turkish Angora',
  'Turkish Van',
  'Turkish Vankedisi',
  'Ukrainian Levkoy',
  'Wila Krungthep',
  'York Chocolate',
];
