import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:path/path.dart';
import 'package:whatsapp_cw/whatsapp/bloc/bloc_whatsapp/blocwhatsapp_bloc.dart';
import 'package:whatsapp_cw/whatsapp/model/model_group.dart';
import 'package:whatsapp_cw/whatsapp/utils/user_preference.dart';
import 'package:whatsapp_cw/widgets/card_highlight.dart';
import 'package:file_picker/file_picker.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/link.dart';
import 'package:uuid/uuid.dart';
import 'package:whatsapp_cw/widgets/deferred_widget.dart';

import '../models/sponsor.dart';
import '../widgets/changelog.dart';
import '../widgets/material_equivalents.dart';
import '../widgets/page.dart';
import '../widgets/sponsor.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dart:io' as Io;
import 'package:flutter/services.dart' show rootBundle;

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:path_provider/path_provider.dart';

import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter_markdown/flutter_markdown.dart'
    deferred as flutter_markdown;

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with PageMixin {
  String? comboboxValue;
  BlocwhatsappBloc _bloc = BlocwhatsappBloc();
  bool _firstOpen = true;
  InfoBarSeverity severity = InfoBarSeverity.info;

  final secondController = ScrollController();
  List<Lista> selected = [];
  final shuffledIcons = FluentIcons.allIcons.values.toList()..shuffle();

  late List<Lista> contacts = [];

  final TextEditingController _controller = TextEditingController();
  final TextEditingController _controllerLog = TextEditingController();
  final TextEditingController _controllerMensaje = TextEditingController();
  final TextEditingController _controllerMensaje2 = TextEditingController();
  final TextEditingController _controllerDescription = TextEditingController();

  final List<FilesPhoto> _cargos = <FilesPhoto>[];
  final List<FilesPhoto> _cargosPDF = <FilesPhoto>[];

  late Io.File file = Io.File("");

  var uuid = const Uuid();

  bool _descriptionStatus = false;

  Future<void> _openCamera(
    int cameraPosition,
  ) async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['jpg', 'png', 'jpeg'],
      );

      if (result != null) {
        final String base64;

        file = Io.File(result.files.single.path!);

        
        base64 = base64Encode(file.readAsBytesSync());

        setState(() {});
      }

      final data =
          _cargos.firstWhere((element) => element.id == cameraPosition);

      if (data != null) {
        data.img = file;
        data.b64 = base64Encode(await file.readAsBytes());
        data.name = file.path.split(Platform.pathSeparator).last;
        log("${data.name}");
      } else {}

      setState(() {});
    } catch (e) {
      print(
          "ERROR::::::::::::::::::::::::::::::::::::::::::::: ${e.toString()}");
    }
  }

  Future<void> _openPDF(
    int position,
  ) async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'PDF'],
      );
      if (result != null) {
        file = Io.File(result.files.single.path!);
        setState(() {});
      }
      final data = _cargosPDF.firstWhere((element) => element.id == position);

      if (data != null) {
        ByteData imageData1 = await rootBundle.load('assets/pdf.png');
        Uint8List audioUint8List = imageData1.buffer
            .asUint8List(imageData1.offsetInBytes, imageData1.lengthInBytes);

        data.img = File.fromRawPath(audioUint8List);
        data.b64 = base64Encode(await file.readAsBytes());
        data.name = file.path.split(Platform.pathSeparator).last;
        data.name2 = "${uuid.v4()}.pdf";
      } else {}

      setState(() {});
    } catch (e) {
      print("ERROR::::: ${e.toString()}");
    }
  }

  UserPreferences _pref = UserPreferences();
  @override
  void initState() {
    log("--------SIEMPRE AL INICIAR LA APP.........");
    _bloc.add(GetAuth());

    _cargos.add(FilesPhoto(id: 1, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 2, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 3, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 4, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 5, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 6, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 7, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 8, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 9, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 10, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 11, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 12, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 13, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 14, img: null, b64: "", name2: ""));
    _cargos.add(FilesPhoto(id: 15, img: null, b64: "", name2: ""));

    _cargosPDF.add(FilesPhoto(id: 1, img: null, b64: "", name: "", name2: ""));
    _cargosPDF.add(FilesPhoto(id: 2, img: null, b64: "", name: "", name2: ""));

    //fileCamare = Io.File(await lo);
    _pref.countTime = 12;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasFluentTheme(context));
    final theme = FluentTheme.of(context);

    return BlocProvider.value(
      value: _bloc,
      child: ScaffoldPage.scrollable(
        header: BlocBuilder<BlocwhatsappBloc, BlocwhatsappState>(
          builder: (context, state) {
            //contacts = (state.modelGroup!.lista==null)?[]:state.modelGroup!.lista!;
            return PageHeader(
              //title: Text('Envio masivo Whatsapp CW ${state.modelGroup!.lista!.length}'),
              title: Text(
                ' Whatsapp CW -  ${(state.modelGroup!.lista != null) ? state.modelGroup!.lista!.length : 0} grupos:(${selected.length})   PORT-DB: ${state.info}   TIME: ${_pref.countTime}',
                style: TextStyle(fontWeight: FontWeight.w200, fontSize: 16),
              ),
              commandBar: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [],
              ),
            );
          },
        ),
        children: [
          BlocBuilder<BlocwhatsappBloc, BlocwhatsappState>(
              builder: (context, state) {
            _controllerLog.text = state.printLog!;
            return Card(
              child: Wrap(
                alignment: WrapAlignment.center,
                spacing: 10.0,
                children: [
                  // Container(
                  //   width: double.infinity,
                  //   child: TextBox(
                  //     controller: _controllerLog,
                  //     //header: 'NOMBRES:',
                  //     placeholder: 'Log',
                  //     expands: false,
                  //     maxLines: null,
                  //     autofocus: true,
                  //     //cursorColor: Colors.red,

                  //     prefix: Container(
                  //       padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                  //       child: Icon(FluentIcons.user_window, size: 22.0),
                  //     ),
                  //     //highlightColor: Colors.green,
                  //     unfocusedColor: Colors.transparent,
                  //   ),
                  // ),
                  if (state.statusAuth == StatusAuth.estadoConectando)
                    Container(
                      child: ProgressBar(),
                    ),
                  if (state.statusAuth == StatusAuth.estadoQR)
                    Container(
                      height: 220,
                      width: 220,
                      child: QrImage(
                        data: state.modelQr!.qr!,
                        version: QrVersions.auto,
                        size: 320,
                        backgroundColor: Colors.white,
                        gapless: false,
                        embeddedImage: const AssetImage('assets/174879.png'),
                        embeddedImageStyle: QrEmbeddedImageStyle(
                          size: const Size(40, 40),
                        ),
                      ),
                    ),
                  if (state.statusAuth == StatusAuth.estadoConectado)
                    InfoBar(
                      title: Text('Estado del dispositivo '),
                      content: Text(
                        'Ya existe un número sincronizado ( ${state.modelAuth!.info!.number!} ), puede seguir o desconectar para sincronizar con otro número.',
                      ),
                      severity: InfoBarSeverity.success,
                      isLong: true,
                      onClose: () => setState(() => _firstOpen = false),
                    ),
                ],
              ),
            );
          }),
          subtitle(
            content: const Text(''),
          ),
          BlocBuilder<BlocwhatsappBloc, BlocwhatsappState>(
              builder: (context, state) {
            return (state.statusEnvioSMJ == true)
                ? ContentDialog(
                    style: const ContentDialogThemeData(
                        padding: EdgeInsets.all(30)),
                    constraints: const BoxConstraints(maxWidth: 600),
                    content: () {
                      return Column(
                        children: [
                          const ProgressRing(),
                          Text(
                              "Enviando Mensajes ${state.countSend} de ${(state.modelGroup!.lista != null) ? state.modelGroup!.lista!.length : 0}")
                        ],
                      );
                    }(),
                  )
                : CardHighlight(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        if (state.statusAuth == StatusAuth.estadoConectado)
                          //------------------------------------------------------------ TEXTO DE FIRLTRO Y BUSQUEDA
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Expanded(
                                child: TextBox(
                                  controller: _controller,
                                  //header: 'NOMBRES:',
                                  placeholder: 'Filtrar grupos',
                                  expands: false,
                                  // onEditingComplete: () {
                                  //   log("------ onEditingComplete");
                                  // },
                                  // onChanged: (v) {
                                  //   log("------ onChanged $v");
                                  // },
                                  // onSubmitted: (value) {
                                  //   log("------ onSubmitted $value");
                                  // },
                                  // onTap: () {
                                  //   log("------ onTap");
                                  // },
                                  autofocus: true,
                                  //cursorColor: Colors.red,

                                  prefix: Container(
                                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                    child: Icon(FluentIcons.user_window,
                                        size: 22.0),
                                  ),
                                  //highlightColor: Colors.green,
                                  unfocusedColor: Colors.transparent,
                                ),
                              ),
                              SizedBox(width: 10.0),
                              Expanded(
                                  child: Button(
                                child: Text(
                                    'Listar grupos de ${state.modelAuth!.info!.number!} '),
                                onPressed: () {
                                  _bloc.add(GetGroup(fill: _controller.text));
                                },
                              )),
                              // SizedBox(width: 50.0),
                            ],
                          ),
                        const SizedBox(height: 20.0),
                        if (state.modelGroup!.lista != null)
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              //------------------------------------------------------------ LISTA DE GRUPOS
                              Expanded(
                                  child: Column(
                                children: [
                                  Container(
                                    height: 580,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: theme.resources
                                            .surfaceStrokeColorDefault,
                                      ),
                                    ),
                                    child: ListView.builder(
                                      controller: secondController,
                                      shrinkWrap: true,
                                      itemCount: state.modelGroup!.lista!
                                          .length, //contacts.length,
                                      itemBuilder: (context, index) {
                                        final contact =
                                            state.modelGroup!.lista![index];
                                        return ListTile.selectable(
                                          leading: const CircleAvatar(
                                              backgroundImage: AssetImage(
                                                  'assets/174879.png'),
                                              backgroundColor: Colors.white,
                                              radius: 15.0),
                                          title: Text(
                                            "${contact.name}",
                                            style: const TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          subtitle: Text("${contact.create}"),
                                          trailing: Text(
                                              "${contact.participants!.length}👨‍👩‍👦‍👦"),
                                          selectionMode:
                                              ListTileSelectionMode.multiple,
                                          selected: selected.contains(contact),
                                          onSelectionChange: (selected) {
                                            log("${state.modelGroup!.lista![index].name}");
                                            setState(() {
                                              if (selected) {
                                                this.selected.add(contact);
                                              } else {
                                                this.selected.remove(contact);
                                              }
                                            });
                                            this.selected.forEach((element) {
                                              print("----${element.name}");
                                            });
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              )),
                              //------------------------------------------------------------ IMGENES / PDF /MENSJ /

                              Expanded(
                                child: CardHighlight(
                                  codeSnippet: '-',
                                  child: Column(
                                    children: [
                                      Center(
                                        child: Text("Mensaje"),
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: TextBox(
                                              maxLines: null,
                                              controller: _controllerMensaje,
                                              //header: 'NOMBRES:',
                                              placeholder: 'Filtrar grupos',
                                              expands: false,
                                              // onEditingComplete: () {
                                              //   log("------ onEditingComplete");
                                              // },
                                              // onChanged: (v) {
                                              //   log("------ onChanged $v");
                                              // },
                                              // onSubmitted: (value) {
                                              //   log("------ onSubmitted $value");
                                              // },
                                              // onTap: () {
                                              //   log("------ onTap");
                                              // },
                                              autofocus: true,
                                              //cursorColor: Colors.red,

                                              prefix: Container(
                                                padding: EdgeInsets.fromLTRB(
                                                    5, 0, 5, 0),
                                                child: Icon(
                                                    FluentIcons.pen_workspace,
                                                    size: 22.0),
                                              ),
                                              //highlightColor: Colors.green,
                                              unfocusedColor:
                                                  Colors.transparent,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Center(
                                        child: Text("Adjuntar imagenes"),
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  1,
                                                )
                                              },
                                              file: _cargos[0].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  2,
                                                )
                                              },
                                              file: _cargos[1].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  3,
                                                )
                                              },
                                              file: _cargos[2].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  4,
                                                )
                                              },
                                              file: _cargos[3].img,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  5,
                                                )
                                              },
                                              file: _cargos[4].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  6,
                                                )
                                              },
                                              file: _cargos[5].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  7,
                                                )
                                              },
                                              file: _cargos[6].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  8,
                                                )
                                              },
                                              file: _cargos[7].img,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  9,
                                                )
                                              },
                                              file: _cargos[8].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  10,
                                                )
                                              },
                                              file: _cargos[9].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  11,
                                                )
                                              },
                                              file: _cargos[10].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  12,
                                                )
                                              },
                                              file: _cargos[11].img,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  13,
                                                )
                                              },
                                              file: _cargos[12].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  14,
                                                )
                                              },
                                              file: _cargos[13].img,
                                            ),
                                          ),
                                          SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              active: true,
                                              onPressed: () => {
                                                _openCamera(
                                                  15,
                                                )
                                              },
                                              file: _cargos[14].img,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Center(
                                        child: Text("Mensaje"),
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: TextBox(
                                              maxLines: null,
                                              controller: _controllerMensaje2,
                                              //header: 'NOMBRES:',
                                              placeholder: 'Filtrar grupos',
                                              expands: false,
                                              // onEditingComplete: () {
                                              //   log("------ onEditingComplete");
                                              // },
                                              // onChanged: (v) {
                                              //   log("------ onChanged $v");
                                              // },
                                              // onSubmitted: (value) {
                                              //   log("------ onSubmitted $value");
                                              // },
                                              // onTap: () {
                                              //   log("------ onTap");
                                              // },
                                              autofocus: true,
                                              //cursorColor: Colors.red,

                                              prefix: Container(
                                                padding: EdgeInsets.fromLTRB(
                                                    5, 0, 5, 0),
                                                child: Icon(
                                                    FluentIcons.pen_workspace,
                                                    size: 22.0),
                                              ),
                                              //highlightColor: Colors.green,
                                              unfocusedColor:
                                                  Colors.transparent,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                        height: 10,
                                      ),
                                      Center(
                                        child: Text("Adjuntar Pdf"),
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Gallery(
                                              type: "pdf",
                                              active: true,
                                              onPressed: () {
                                                // print("${fileCamare}");
                                                _openPDF(
                                                  1,
                                                );
                                              },
                                              file: _cargosPDF[0].img,
                                            ),
                                          ),
                                          const SizedBox(width: 10.0),
                                          Expanded(
                                            child: Gallery(
                                              type: "pdf",
                                              active: true,
                                              onPressed: () => {
                                                _openPDF(
                                                  2,
                                                )
                                              },
                                              file: _cargosPDF[1].img,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 22.0),
                                      Row(
                                        children: [
                                          Expanded(
                                              child: FilledButton(
                                            child: const Text('Enviar'),
                                            onPressed: () {
                                              _descriptionStatus = false;
                                              setState(() {});
                                              showContentDialog(context);
                                            },
                                          )),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                      ],
                    ),
                    codeSnippet: '-',
                  );
          }),
        ],
      ),
    );
  }

  void showContentDialog(BuildContext context) async {
    final result = await showDialog<String>(
      context: context,
      builder: (context) => ContentDialog(
        title: const Text('📨 📧 🙂'),
        content: Container(
          height: 60,
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Asigne una descripcion al envío.',
              ),
              Expanded(
                child: TextBox(
                  controller: _controllerDescription,
                  //header: 'NOMBRES:',
                  placeholder: 'Filtrar grupos',
                  expands: false,
                  autofocus: true,
                  // cursorColor: (_descriptionStatus) ? Colors.red : Colors.blue,
                  highlightColor: (_descriptionStatus) ? Colors.red : null,
                  prefix: Container(
                    padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                    child: const Icon(FluentIcons.pencil_reply, size: 22.0),
                  ),
                  //highlightColor: Colors.green,
                  unfocusedColor: Colors.transparent,
                ),
              ),
            ],
          ),
        ),
        actions: [
          Button(
            child: const Text('Si'),
            onPressed: () {
              // _cargos.forEach((element) {
              //   if (element.b64 == "") {
              //     log("-->vacio");
              //   } else {
              //     log("--> dato");
              //   }
              // });

              // _cargosPDF.forEach((element) {
              //   if (element.b64 == "") {
              //     log("----**>vacio");
              //   } else {
              //     log("----**>dato");
              //   }
              // });
              print("------>${_controllerDescription.text}*");

              if (_controllerDescription.text != "") {
                _bloc.add(
                  SendMesaje(
                    filePDF: _cargosPDF,
                    filePHOTO: _cargos,
                    mensajeTEXT: _controllerMensaje.text,
                    mensajeTEXT2: _controllerMensaje2.text,
                    description: _controllerDescription.text,
                    context: context,
                    selected: selected,
                  ),
                );
                Navigator.pop(context, 'close');
              } else {
                displayInfoBar(context, builder: (context, close) {
                  return InfoBar(
                    title: const Text('Se requiere,'),
                    content: const Text(
                        'una breve descripción para el grupo de envío.'),
                    action: IconButton(
                      icon: const Icon(FluentIcons.clear),
                      onPressed: close,
                    ),
                    severity: InfoBarSeverity.warning,
                  );
                });
              }

              // Delete file here
            },
          ),
          FilledButton(
            child: const Text('Cancel'),
            onPressed: () => Navigator.pop(context, 'User canceled dialog'),
          ),
        ],
      ),
    );
    setState(() {});
  }
}

class SponsorButton extends StatelessWidget {
  const SponsorButton({
    Key? key,
    required this.imageUrl,
    required this.username,
  }) : super(key: key);

  final String imageUrl;
  final String username;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        height: 60,
        width: 60,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(imageUrl),
          ),
          shape: BoxShape.circle,
        ),
      ),
      Text(username),
    ]);
  }
}

class Gallery extends StatefulWidget {
  final void Function()? onPressed;
  final Io.File? file;
  final bool? active;
  final String? type;
  Gallery({
    Key? key,
    this.onPressed,
    this.file,
    this.active,
    this.type,
  }) : super(key: key);

  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: double.infinity,
      child: widget.file == null
          ? Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  icon: (widget.active!)
                      ? const Icon(FluentIcons.camera, size: 27.0)
                      : const Icon(FluentIcons.photo, size: 27.0),
                  onPressed: (widget.active!) ? widget.onPressed : null,
                  // color: Colors.green,
                ),
              ],
            )
          : Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: (widget.type == "pdf")
                      ? Container(
                          decoration: const BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/pdf.png'),
                            ),
                            shape: BoxShape.circle,
                          ),
                        )
                      : Image.file(
                          widget.file!,
                          // height: 85,
                          // width: 85,
                          fit: BoxFit.cover,
                        ),
                ),
                CircleWithIcon(
                  function: widget.onPressed!,
                  //icon: const Icon(FluentIcons.edit, size: 24.0),
                  coloricon: Colors.black,
                ),
              ],
            ),
      decoration: BoxDecoration(
          color: Colors.black,
          border: Border.all(
            color: Colors.blue, // Set border color
            width: 1.0,
          ), // Set border width
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ), // Set rounded corner radius
          boxShadow: [
            BoxShadow(
              blurRadius: 1,
              color: Colors.purple,
              offset: Offset(0, 0),
            )
          ] // Make rounded corner of border
          ),
    );
  }
}

class CircleWithIcon extends StatelessWidget {
  final IconData? icon;
  final void Function()? function;
  final Color? coloricon;

  CircleWithIcon(
      {@required this.icon, @required this.function, @required this.coloricon});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      child: Container(
        width: 40,
        height: 40,
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: IconButton(
          icon: Icon(
            FluentIcons.pencil_reply,
            size: 24.0,
            color: Colors.black,
          ),
          onPressed: function,
        ),
      ),
      right: 5,
      top: 5,
    );
  }
}

class FilesPhoto {
  int? id;
  Io.File? img;
  String? b64;
  String? name;
  String? name2;

  FilesPhoto({this.id, this.img, this.b64, this.name, this.name2});
}
