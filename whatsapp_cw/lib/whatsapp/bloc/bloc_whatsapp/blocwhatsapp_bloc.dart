import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/services.dart';
import 'package:whatsapp_cw/screens/home.dart';
import 'package:whatsapp_cw/whatsapp/api/api_whatsapp.dart';
import 'package:whatsapp_cw/whatsapp/database/data_base_provider.dart';
import 'package:whatsapp_cw/whatsapp/model/mdoel_exel_retirados.dart';
import 'package:whatsapp_cw/whatsapp/model/model_auth.dart';
import 'package:whatsapp_cw/whatsapp/model/model_config.dart';
import 'package:whatsapp_cw/whatsapp/model/model_envio_list.dart';
import 'package:whatsapp_cw/whatsapp/model/model_exel_envio.dart';
import 'package:whatsapp_cw/whatsapp/model/model_group.dart';
import 'package:whatsapp_cw/whatsapp/model/model_qr.dart';
import 'package:whatsapp_cw/whatsapp/model/model_response_send.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:uuid/uuid.dart';
import 'package:whatsapp_cw/whatsapp/model/model_rs.dart';
import 'package:whatsapp_cw/whatsapp/utils/generate_exel.dart';
import 'package:whatsapp_cw/whatsapp/utils/generate_exel_existencia.dart';
import 'package:whatsapp_cw/whatsapp/utils/user_preference.dart';
import 'package:whatsapp_cw/widgets/dialog.dart';

part 'blocwhatsapp_event.dart';
part 'blocwhatsapp_state.dart';

final ApiWhatsapp _api = ApiWhatsapp();

class BlocwhatsappBloc extends Bloc<BlocwhatsappEvent, BlocwhatsappState> {
  BlocwhatsappBloc() : super(BlocwhatsappState.inititalState) {
    Timer timer;
    int contado = 0;
    var uuid = const Uuid();
    String? result = '';
    UserPreferences _pre = UserPreferences();
    int timeSend = 8;
    BuildContext _contex;
    timerOuth() async {
      timer = Timer.periodic(const Duration(seconds: 5), (Timer t) async {
        if (state.statusAuth != StatusAuth.estadoConectado) {
          ModelAuth _modelAuth = ModelAuth();
          ModelQr _modelQr = ModelQr();

          _modelAuth = await _api.getAuth();

          if (_modelAuth.message == "CONNECTED") {
            emit(state.copyWith(
              modelAuth: _modelAuth,
              statusAuth: StatusAuth.estadoConectado,
            ));
          } else if (_modelAuth.message == "DISCONNECTED") {
            _modelQr = await _api.getQR();
            emit(state.copyWith(
              modelQr: _modelQr,
              statusAuth: StatusAuth.estadoQR,
            ));
          }
        }
        log("ESTADO CONECIÓN: ${state.statusAuth}");
      });
    }

    on<Prueba>((event, emit) {
      log("message ${event.prueba}");
      emit(state.copyWith(statusAuth: state.statusAuth));
    });

    on<GetAuth>((event, emit) async {
      if (_pre.port == "" || _pre.port == null) {
        String data = await rootBundle.loadString('assets/config.json');
        ModelConfig _model = ModelConfig();
        _model = ModelConfig.fromJson(json.decode(data));
        timeSend = _model.time!;
        _pre.countTime = timeSend;
        state.info =
            "${_model.hostserver} - ${_model.db!.split(".")[0].trim()}";
      }

      log("Event: GetAuth");
      emit(state.copyWith(
          statusAuth: StatusAuth.estadoConectando, info: state.info));
      ModelRs _modelRs = ModelRs();
      _modelRs = await _api.getRS();
      log(_modelRs.status.toString());
      if (_modelRs.status != "fail") {
        timerOuth();
      } else {
        log("El edmin bloqueo el sistema...");
      }
    });

    on<GetGroup>((event, emit) async {
      ModelGroups _modelGroup = ModelGroups();

      _modelGroup = await _api.getGroup(event.fill ?? "");

      if (_modelGroup.status == "correcto") {
        emit(state.copyWith(
          modelGroup: _modelGroup,
        ));
      } else {
        log("${_modelGroup.error}");
      }
    });

    on<SendMesaje>((event, emit) async {
      emit(state.copyWith(
          statusEnvioSMJ: true,
          countSend: 0,
          printLog: "Inicia enviado mensaje ..... \n"));

      try {
        final dbprovider = DatabaseProvider.db;
        final db = await dbprovider.database;
        String idSql = uuid.v4();
        log("SQLITE correcto...");

        if (event.selected!.isNotEmpty) {
          state.modelGroup!.lista = event.selected;
          emit(state.copyWith(
            modelGroup: state.modelGroup,
          ));
        }
        try {
          // var value = {
          //   "envio_id": idSql,
          //   "envio_description": event.description ?? "-",
          //   "envio_jsonData": jsonEncode(state.modelGroup),
          //   "envio_jsonlOG": " ",
          //   "envio_number": state.modelAuth!.info!.number!,
          //   "envio_admin": "mario-admin",
          //   "envio_status": 0
          // };
          await db.query(
              'insert into t_envio  (envio_id,envio_description,envio_jsonData,envio_jsonlOG,envio_number,envio_admin,envio_status) values (?,?,?,?,?,?,?)',
              [
                idSql,
                event.description ?? "-",
                jsonEncode(state.modelGroup).toString(),
                ' ',
                state.modelAuth!.info!.number!,
                'mario-admin',
                0
              ]);

          emit(state.copyWith(
              printLog: "${state.printLog} se guado en SQL ..... \n"));

          //________________________ MINIFICAR PDF

          for (var fileMini in event.filePDF!) {
            emit(state.copyWith(
                printLog: "${state.printLog} se envio minificar pdf ..... \n"));
            if (fileMini.b64 != "") {
              ModelResponseSend model = ModelResponseSend();
              model = await _api.sendPdfMini(fileMini.b64!, fileMini.name2!);
            }
          }
          int h = 0;
          emit(state.copyWith(
              printLog:
                  "${state.printLog}  Por enviar a grupos ${state.modelGroup!.lista!.length} ..... \n"));
          for (var element1 in state.modelGroup!.lista!) {
            h = h + 1;
            //__________________________ msj1
            ModelResponseSend model3 = ModelResponseSend();
            model3 = await _api.sendMsj(
              element1.id!,
              event.mensajeTEXT!,
            );
            emit(state.copyWith(
                printLog: "${state.printLog} $h Mensaje 1 enviado ..... \n"));
            // //__________________________ img
            for (var element3 in event.filePHOTO!) {
              if (element3.b64 != "") {
                ModelResponseSend model2 = ModelResponseSend();
                model2 = await _api.sendImg(
                  id: element1.id!,
                  img: element3.b64!,
                );
              }
            }
            emit(state.copyWith(
                printLog: "${state.printLog} $h Imagenes  enviado ..... \n"));
            //__________________________ msj1
            ModelResponseSend model34 = ModelResponseSend();
            model34 = await _api.sendMsj(
              element1.id!,
              event.mensajeTEXT2!,
            );
            emit(state.copyWith(
                printLog: "${state.printLog} $h Mensaje 2 enviado ..... \n"));
            //__________________________ pdf
            for (var element2 in event.filePDF!) {
              if (element2.b64 != "") {
                ModelResponseSend model1 = ModelResponseSend();
                model1 = await _api.sendPdf(
                  id: element1.id!,
                  pdfName: element2.name!,
                  pdfName2: element2.name2!,
                );
              }
            }
            emit(state.copyWith(
                printLog: "${state.printLog} $h Pdf  enviado ..... \n"));

            element1.status = 1;

            await db.query(
                'update t_envio  set envio_jsonlOG = ? where envio_id = ?',
                [jsonEncode(state.modelGroup), idSql]);

            emit(state.copyWith(
              countSend: h,
            ));
           // log("--ENVIADO A : $h  ${element1.name} ");

            await Future.delayed(Duration(seconds: timeSend));
          }

          await db.query(
              'update t_envio  set envio_status  = 1 where envio_id = ?',
              [idSql]);

          db.close();
        } catch (exception) {
          final dbprovider = DatabaseProvider.db;
          final db = await dbprovider.database;
          await db.query(
              'update t_envio  set envio_jsonlOG = ? where envio_id = ?',
              [jsonEncode(state.modelGroup), idSql]);
          db.close();
          print("---- rrrrr $exception");
        }
      } catch (e) {
        emit(state.copyWith(printLog: "${state.printLog}  ${e.toString()} \n"));

        log("error: ${e}");

        displayInfoBar(event.context!, builder: (context, close) {
          return InfoBar(
            title: const Text('Error,'),
            content: Text("Error Dase de Datos ${e.toString()}"),
            action: IconButton(
              icon: const Icon(FluentIcons.clear),
              onPressed: close,
            ),
            severity: InfoBarSeverity.error,
          );
        });
      }

      emit(state.copyWith(
        statusEnvioSMJ: false,
      ));
    });

    on<GetReportEnvio>(
      (event, emit) async {
        final dbprovider = DatabaseProvider.db;
        final db = await dbprovider.database;

        var maps = await db.query(
            "select  * from t_envio te where te.envio_number =? order by envio_date desc ",
            [event.number]);
        db.close();
        List<ModeExelEnvio> listEnvio = [];

        String dataRow = "";

        for (var row in maps) {
          ModeExelEnvio raw = ModeExelEnvio();

          raw.envioId = row[0].toString();
          raw.envioDescription = row[1].toString();
          raw.envioJsonData = row[2].toString();
          raw.envioJsonlOg = row[3].toString();
          raw.envioNumber = row[4].toString();
          raw.envioAdmin = row[5].toString();
          raw.envioStatus = row[6];
          raw.envioDate = row[7];
          listEnvio.add(raw);
        }

        //_modelGroupBAK = ModelGroups.fromJson(json.decode(_map["data"]));

        // listEnvio = modeExelEnvioFromJson(jsonEncode(maps));

        generateExcel(listEnvio);
      },
    );

    on<EnvioSelect>(
      (event, emit) async {
        print("------> ${event.envioSelect}");
        emit(
          state.copyWith(envioSelect: event.envioSelect),
        );
      },
    );

    on<GetEnvioList>(
      (event, emit) async {
        print("------> consulta de lista");

        final dbprovider = DatabaseProvider.db;
        final db = await dbprovider.database;
        print("------> consulta de lista2");
        var maps = await db.query(
            "select  envio_date ,envio_description  from t_envio  where envio_number =? order by envio_date desc",
            [event.number]);

        db.close();
        print("------> consulta de lista3 ${maps.toString()}");
        List<ModelEnvioList> listEnvio = [];

        for (var row in maps) {
          ModelEnvioList rsul = ModelEnvioList();
          rsul.envioDate = row[0].toString();
          rsul.envioDescription = row[1].toString();
          listEnvio.add(rsul);
        }

        // listEnvio = modelEnvioListFromJson(jsonEncode(maps.toString()));

        emit(state.copyWith(listEnvio: listEnvio));
      },
    );

    on<GenerateExelExistencias>(
      (event, emit) async {
        emit(state.copyWith(
          statusExelExistencia: true,
        ));
        int count = 0;
        final dbprovider = DatabaseProvider.db;
        final db = await dbprovider.database;

        ModelGroups _modelGroupBAK = ModelGroups();
        ModelGroups _modelGroupNEW = ModelGroups();

        var maps = await db.query(
          "select  te.envio_jsonData  from t_envio te where te.envio_number =? and te.envio_date =?",
          [event.number, event.envioDate.envioDate],
        );
        db.close();
        String dataRow = "";

        for (var row in maps) {
          dataRow = row[0].toString();
        }

        Map<String, dynamic> _map = Map<String, dynamic>();
        _map["data"] = dataRow;

        _modelGroupBAK = ModelGroups.fromJson(json.decode(_map["data"]));
        emit(state.copyWith(
          countGroupExistencias: _modelGroupBAK.lista!.length,
        ));

        /**__________________________________________________________________ */

        _modelGroupNEW = await _api.getGroup("");

        List<ModelExelExistencia> _listModelExelExistencia = [];

        //---------------------------> Sqlite
        if (_modelGroupNEW.status == "correcto") {
          try {
            for (var element1 in _modelGroupBAK.lista!) {
              count = count + 1;
              log("________________BUSCANDO : ${element1.id} ${element1.name} ${element1.participants!.length}");
              // if (count == 14) {
              //   print("--------------veremos el error");
              // }
              ModelExelExistencia _ModelExelExistencia = ModelExelExistencia();

              _ModelExelExistencia.nameGroup = element1.name;
              _ModelExelExistencia.owner =
                  (element1.owner == null) ? "-" : element1.owner!.user;
              //-------------------------> whatsapp
              final data = _modelGroupNEW.lista!.firstWhereOrNull(
                (item) => item.id == element1.id,
              );
              if (data != null) {
                List<Participantt>? participantt = [];
                element1.participants!.forEach((element4) {
                  final data125 = data.participants!.firstWhereOrNull(
                    (item) => element4.id!.user == item.id!.user,
                  );
                  Participantt _modelParticipant = Participantt();
                  if (data125 != null) {
                    _modelParticipant.number = element4.id!.user;
                    _modelParticipant.status = "✅";
                    log("________________ENCONTRADO : ${data.id} ${data.name} ${data.participants!.length}  ${element4.id!.user} ✅");
                  } else {
                    _modelParticipant.number = element4.id!.user;
                    _modelParticipant.status = "❌";
                    log("_____________NO___ENCONTRADO : ${data.id} ${data.name} ${data.participants!.length}  ${element4.id!.user} ❌");
                  }
                  participantt.add(_modelParticipant);
                });
                _ModelExelExistencia.participantt = participantt;
              }

              _listModelExelExistencia.add(_ModelExelExistencia);

              emit(state.copyWith(
                countExitencias: count,
              ));

              log("--ENVIADO A : $count  ${element1.name} ");
              await Future.delayed(const Duration(seconds: 2));
            }
            generateExcelExistencia(_listModelExelExistencia, event);
          } catch (e) {
            log("-----------------------2 $e");
            displayInfoBar(event.context, builder: (context, close) {
              return InfoBar(
                title: const Text('Error al procezar la data,'),
                content: Text(e.toString()),
                action: IconButton(
                  icon: const Icon(FluentIcons.clear),
                  onPressed: close,
                ),
                isLong: true,
                severity: InfoBarSeverity.error,
              );
            });
          }
        } else {
          log("-----------------------1 ${_modelGroupNEW.error}");
          displayInfoBar(event.context, builder: (context, close) {
            return InfoBar(
              title: const Text('Se tiene el Siguiente errr interno,'),
              content: Text("${_modelGroupNEW.error}"),
              action: IconButton(
                icon: const Icon(FluentIcons.clear),
                onPressed: close,
              ),
              severity: InfoBarSeverity.error,
            );
          });
        }

        emit(state.copyWith(
          statusExelExistencia: false,
        ));
      },
    );
  }
}
