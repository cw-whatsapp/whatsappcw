part of 'blocwhatsapp_bloc.dart';

enum StatusAuth {
  estadoConectando,
  estadoConectado,
  estadoDesconectado,
  estadoQR,
}

@immutable
class BlocwhatsappState {
  final StatusAuth? statusAuth;
  final ModelAuth? modelAuth;
  final ModelQr? modelQr;
  final String? hashWhatsapp;
  final ModelGroups? modelGroup;
  final String? envioSelect;
  final List<ModelEnvioList>? listEnvio;
  final int? countSend;
  final bool? statusEnvioSMJ;
  final int? countGroupExistencias;

  final bool? statusExelExistencia;
  final int? countExitencias;
  String? info;
  String? printLog;
  ModelConfig? conexion;

  BlocwhatsappState({
    this.statusAuth,
    this.modelAuth,
    this.hashWhatsapp,
    this.modelQr,
    this.modelGroup,
    this.envioSelect,
    this.listEnvio,
    this.statusEnvioSMJ,
    this.countSend,
    this.statusExelExistencia,
    this.countExitencias,
    this.countGroupExistencias,
    this.info,
    this.printLog,
    this.conexion,
  });
  static BlocwhatsappState get inititalState => BlocwhatsappState(
        hashWhatsapp: "",
        modelAuth: ModelAuth(),
        modelGroup: ModelGroups(),
        modelQr: ModelQr(),
        statusAuth: StatusAuth.estadoDesconectado,
        envioSelect: "",
        listEnvio: const [],
        statusEnvioSMJ: false,
        countSend: 0,
        statusExelExistencia: false,
        countExitencias: 0,
        countGroupExistencias: 0,
        info: "-",
        printLog: "",
        conexion: ModelConfig(),
      );

  BlocwhatsappState copyWith({
    StatusAuth? statusAuth,
    ModelAuth? modelAuth,
    ModelQr? modelQr,
    String? hashWhatsapp,
    ModelGroups? modelGroup,
    String? envioSelect,
    List<ModelEnvioList>? listEnvio,
    bool? statusEnvioSMJ,
    int? countSend,
    bool? statusExelExistencia,
    int? countExitencias,
    int? countGroupExistencias,
    String? info,
    String? printLog,
    ModelConfig? conexion,
  }) {
    return BlocwhatsappState(
      statusAuth: statusAuth ?? this.statusAuth,
      modelAuth: modelAuth ?? this.modelAuth,
      modelQr: modelQr ?? this.modelQr,
      hashWhatsapp: hashWhatsapp ?? this.hashWhatsapp,
      modelGroup: modelGroup ?? this.modelGroup,
      envioSelect: envioSelect ?? this.envioSelect,
      listEnvio: listEnvio ?? this.listEnvio,
      statusEnvioSMJ: statusEnvioSMJ ?? this.statusEnvioSMJ,
      countSend: countSend ?? this.countSend,
      statusExelExistencia: statusExelExistencia ?? this.statusExelExistencia,
      countExitencias: countExitencias ?? this.countExitencias,
      countGroupExistencias:
          countGroupExistencias ?? this.countGroupExistencias,
      info: info ?? this.info,
      printLog: printLog ?? this.printLog,
      conexion: conexion ?? this.conexion,
    );
  }
}
