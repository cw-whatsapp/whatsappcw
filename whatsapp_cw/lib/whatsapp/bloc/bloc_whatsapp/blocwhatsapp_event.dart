part of 'blocwhatsapp_bloc.dart';

@immutable
abstract class BlocwhatsappEvent {}

class Prueba extends BlocwhatsappEvent {
  final String? prueba;

  Prueba({this.prueba});
}

class GetAuth extends BlocwhatsappEvent {}

class GetGroup extends BlocwhatsappEvent {
  final String? fill;

  GetGroup({this.fill});
}

class SendMesaje extends BlocwhatsappEvent {
  final List<FilesPhoto>? filePHOTO;
  final List<FilesPhoto>? filePDF;
  final String? mensajeTEXT;
  final String? mensajeTEXT2;
  final String? description;
  final BuildContext? context;
  final List<Lista>? selected;

  SendMesaje({
    this.filePHOTO,
    this.filePDF,
    this.mensajeTEXT,
    this.mensajeTEXT2,
    this.description,
    this.context,
    this.selected,
  });
}

class GetReportEnvio extends BlocwhatsappEvent {
  final BuildContext context;
  final String number;

  GetReportEnvio({required this.context, required this.number});
}

class EnvioSelect extends BlocwhatsappEvent {
  final String? envioSelect;

  EnvioSelect({this.envioSelect});
}

class GetEnvioList extends BlocwhatsappEvent {
  final String number;

  GetEnvioList({required this.number});
}

class GenerateExelExistencias extends BlocwhatsappEvent {
  final ModelEnvioList envioDate;
  final BuildContext context;
  final String number;

  GenerateExelExistencias({
    required this.envioDate,
    required this.context,
    required this.number,
  });
}
