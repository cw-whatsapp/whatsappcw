import 'dart:convert';

ModelResponseSend modelResponseSendFromJson(String str) =>
    ModelResponseSend.fromJson(json.decode(str));

String modelResponseSendToJson(ModelResponseSend data) =>
    json.encode(data.toJson());

class ModelResponseSend {
  ModelResponseSend({
    this.status,
    this.message,
    this.error,
  });

  String? status;
  String? message;
  String? error;

  ModelResponseSend.error(String error) {
    error = error;
  }

  factory ModelResponseSend.fromJson(Map<String, dynamic> json) =>
      ModelResponseSend(
        status: json["status"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
      };
}
