// To parse this JSON data, do
//
//     final modelExelExistencia = modelExelExistenciaFromJson(jsonString);

import 'dart:convert';

List<ModelExelExistencia> modelExelExistenciaFromJson(String str) =>
    List<ModelExelExistencia>.from(
        json.decode(str).map((x) => ModelExelExistencia.fromJson(x)));

String modelExelExistenciaToJson(List<ModelExelExistencia> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelExelExistencia {
  ModelExelExistencia({
    this.nameGroup,
    this.owner,
    this.participantt,
  });

  String? nameGroup;
  String? owner;
  List<Participantt>? participantt;

  factory ModelExelExistencia.fromJson(Map<String, dynamic> json) =>
      ModelExelExistencia(
        nameGroup: json["nameGroup"],
        owner: json["owner"],
        participantt: List<Participantt>.from(
            json["Participantt"].map((x) => Participantt.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "nameGroup": nameGroup,
        "owner": owner,
        "Participantt": List<dynamic>.from(participantt!.map((x) => x.toJson())),
      };
}

class Participantt {
  Participantt({
    this.number,
    this.status,
  });

  String? number;
  String? status;

  factory Participantt.fromJson(Map<String, dynamic> json) => Participantt(
        number: json["number"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "number": number,
        "status": status,
      };
}
