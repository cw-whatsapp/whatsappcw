// To parse this JSON data, do
//
//     final modelQr = modelQrFromJson(jsonString);

import 'dart:convert';

ModelQr modelQrFromJson(String str) => ModelQr.fromJson(json.decode(str));

String modelQrToJson(ModelQr data) => json.encode(data.toJson());

class ModelQr {
  ModelQr({
    this.status="-",
    this.qr="-",
  });

  String? status;
  String? qr;
  String? error;
  ModelQr.error(String errorMessage) {
    error = errorMessage;
  }

  factory ModelQr.fromJson(Map<String, dynamic> json) => ModelQr(
        status: json["status"],
        qr: json["qr"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "qr": qr,
      };
}
