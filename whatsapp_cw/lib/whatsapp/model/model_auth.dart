import 'dart:convert';

ModelAuth modelAuthFromJson(String str) => ModelAuth.fromJson(json.decode(str));

String modelAuthToJson(ModelAuth data) => json.encode(data.toJson());

class ModelAuth {
  ModelAuth({
    this.status="",
    this.message="",
    this.info,
  });

  String? status;
  String? message;
  Info? info;
  String? error;
  ModelAuth.error(String errorMessage) {
    error = errorMessage;
  }

  factory ModelAuth.fromJson(Map<String, dynamic> json) => ModelAuth(
        status: json["status"],
        message: json["message"],
        info: Info.fromJson(json["info"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "info": info!.toJson(),
      };
}

class Info {
  Info({
    this.name,
    this.number,
  });

  final String? name;
  final String? number;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        name: json["name"],
        number: json["number"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "number": number,
      };
}
