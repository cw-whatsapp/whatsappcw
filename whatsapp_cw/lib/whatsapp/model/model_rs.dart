// To parse this JSON data, do
//
//     final modelRs = modelRsFromJson(jsonString);

import 'dart:convert';

ModelRs modelRsFromJson(String str) => ModelRs.fromJson(json.decode(str));

String modelRsToJson(ModelRs data) => json.encode(data.toJson());

class ModelRs {
  ModelRs({
    this.status,
    this.message,
    this.responseData,
    this.xToken,
    this.db,
    this.color,
  });

  String? status;
  String? message;
  String? responseData;
  String? xToken;
  String? db;
  int? color;
  String? error;

  ModelRs.error(String error) {
    error = error;
  }

  factory ModelRs.fromJson(Map<String, dynamic> json) => ModelRs(
        status: json["status"],
        message: json["message"],
        responseData: json["responseData"],
        xToken: json["x-token"],
        db: json["db"],
        color: json["color"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "responseData": responseData,
        "x-token": xToken,
        "db": db,
        "color": color,
      };
}
