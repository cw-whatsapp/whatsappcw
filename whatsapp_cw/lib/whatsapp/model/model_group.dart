import 'dart:convert';

ModelGroups modelGroupsFromJson(String str) =>
    ModelGroups.fromJson(json.decode(str));

String modeGroupsToJson(ModelGroups data) => json.encode(data.toJson());

class ModelGroups {
  ModelGroups({
    this.status,
    this.lista,
  });

  String? status;
  List<Lista>? lista;
  String? error;
  ModelGroups.error(String error) {
    error = error;
  }

  factory ModelGroups.fromJson(Map<String, dynamic> json) => ModelGroups(
        status: json["status"],
        lista: List<Lista>.from(json["lista"].map((x) => Lista.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "lista": List<dynamic>.from(lista!.map((x) => x.toJson())),
      };
}

class Lista {
  Lista({
    this.id,
    this.name,
    this.create,
    this.owner,
    this.description,
    this.participants,
    this.status,
  });

  String? id;
  String? name;
  int? create;
  Owner? owner;
  String? description;
  List<Participant>? participants;
  int? status;

  factory Lista.fromJson(Map<String, dynamic> json) => Lista(
        id: json["id"],
        name: json["name"],
        create: json["create"],
        owner: json["owner"] == null ? null : Owner.fromJson(json["owner"]),
        description: json["description"],
        participants: List<Participant>.from(
            json["participants"].map((x) => Participant.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "create": create,
        "owner": owner == null ? null : owner!.toJson(),
        "description": description,
        "participants":
            List<dynamic>.from(participants!.map((x) => x.toJson())),
        "status": status,
      };
}

class Owner {
  Owner({
    this.server,
    this.user,
    this.serialized,
  });

  String? server;
  String? user;
  String? serialized;

  factory Owner.fromJson(Map<String, dynamic> json) => Owner(
        server: json["server"],
        user: json["user"],
        serialized: json["_serialized"],
      );

  Map<String, dynamic> toJson() => {
        "server": server,
        "user": user,
        "_serialized": serialized,
      };
}

class Participant {
  Participant({
    this.id,
    this.isAdmin,
    this.isSuperAdmin,
  });

  Owner? id;
  bool? isAdmin;
  bool? isSuperAdmin;

  factory Participant.fromJson(Map<String, dynamic> json) => Participant(
        id: Owner.fromJson(json["id"]),
        isAdmin: json["isAdmin"],
        isSuperAdmin: json["isSuperAdmin"],
      );

  Map<String, dynamic> toJson() => {
        "id": id!.toJson(),
        "isAdmin": isAdmin,
        "isSuperAdmin": isSuperAdmin,
      };
}
