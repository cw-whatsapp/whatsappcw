// To parse this JSON data, do
//
//     final modelConfig = modelConfigFromJson(jsonString);

import 'dart:convert';

ModelConfig modelConfigFromJson(String str) => ModelConfig.fromJson(json.decode(str));

String modelConfigToJson(ModelConfig data) => json.encode(data.toJson());

class ModelConfig {
    ModelConfig({
        this.dbhost,
        this.dbport,
        this.dbuser,
        this.dbpassword,
        this.db,
        this.hostserver,
        this.time,
    });

    String? dbhost;
    int? dbport;
    String? dbuser;
    String? dbpassword;
    String? db;
    String? hostserver;
    int? time;

    factory ModelConfig.fromJson(Map<String, dynamic> json) => ModelConfig(
        dbhost: json["dbhost"],
        dbport: json["dbport"],
        dbuser: json["dbuser"],
        dbpassword: json["dbpassword"],
        db: json["db"],
        hostserver: json["hostserver"],
        time: json["time"],
    );

    Map<String, dynamic> toJson() => {
        "dbhost": dbhost,
        "dbport": dbport,
        "dbuser": dbuser,
        "dbpassword": dbpassword,
        "db": db,
        "hostserver": hostserver,
        "time": time,
    };
}
