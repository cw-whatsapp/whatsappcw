// To parse this JSON data, do
//
//     final modeExelEnvio = modeExelEnvioFromJson(jsonString);

import 'dart:convert';

List<ModeExelEnvio> modeExelEnvioFromJson(String str) => List<ModeExelEnvio>.from(json.decode(str).map((x) => ModeExelEnvio.fromJson(x)));

String modeExelEnvioToJson(List<ModeExelEnvio> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModeExelEnvio {
    ModeExelEnvio({
        this.envioId,
        this.envioDescription,
        this.envioJsonData,
        this.envioJsonlOg,
        this.envioNumber,
        this.envioAdmin,
        this.envioStatus,
        this.envioDate,
    });

    String? envioId;
    String? envioDescription;
    String? envioJsonData;
    String? envioJsonlOg;
    String? envioNumber;
    String? envioAdmin;
    int? envioStatus;
    DateTime? envioDate;

    factory ModeExelEnvio.fromJson(Map<String, dynamic> json) => ModeExelEnvio(
        envioId: json["envio_id"],
        envioDescription: json["envio_description"],
        envioJsonData: json["envio_jsonData"],
        envioJsonlOg: json["envio_jsonlOG"],
        envioNumber: json["envio_number"],
        envioAdmin: json["envio_admin"],
        envioStatus: json["envio_status"],
        envioDate: DateTime.parse(json["envio_date"]),
    );

    Map<String, dynamic> toJson() => {
        "envio_id": envioId,
        "envio_description": envioDescription,
        "envio_jsonData": envioJsonData,
        "envio_jsonlOG": envioJsonlOg,
        "envio_number": envioNumber,
        "envio_admin": envioAdmin,
        "envio_status": envioStatus,
        "envio_date": envioDate!.toIso8601String(),
    };
}
