// To parse this JSON data, do
//
//     final modelEnvioList = modelEnvioListFromJson(jsonString);

import 'dart:convert';

List<ModelEnvioList> modelEnvioListFromJson(String str) => List<ModelEnvioList>.from(json.decode(str).map((x) => ModelEnvioList.fromJson(x)));

String modelEnvioListToJson(List<ModelEnvioList> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelEnvioList {
    ModelEnvioList({
        this.envioDate,
        this.envioDescription,
    });

    String? envioDate;
    String? envioDescription;

    factory ModelEnvioList.fromJson(Map<String, dynamic> json) => ModelEnvioList(
        envioDate: json["envio_date"].toString(),
        envioDescription: json["envio_description"],
    );

    Map<String, dynamic> toJson() => {
        "envio_date": envioDate!.toString(),
        "envio_description": envioDescription,
    };
}
