import 'package:whatsapp_cw/whatsapp/bloc/bloc_whatsapp/blocwhatsapp_bloc.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/material.dart' as m;
import 'package:flutter_bloc/flutter_bloc.dart';

class PageLogin extends StatefulWidget {
  PageLogin({Key? key}) : super(key: key);

  @override
  _PageLoginState createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  BlocwhatsappBloc _bloc = BlocwhatsappBloc();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<String> selectedContacts = [];

    const contacts = ['Kendall', 'Collins', '..'];
    return BlocProvider.value(
      value: _bloc,
      child: m.Scaffold(
        //backgroundColor: m.Colors.white,
        body: BlocBuilder<BlocwhatsappBloc, BlocwhatsappState>(
          builder: (_, state) {
            return Container(
              alignment: AlignmentDirectional.topStart,
              child: Column(
                children: [
                  Button(
                      child: const Text("Increase counter"), onPressed: () {}),
                  Button(
                      child: const Text("Increase counter"), onPressed: () {}),
                  Button(
                      child: const Text("Increase counter"), onPressed: () {}),
                  Button(
                      child: const Text("Increase counter"), onPressed: () {}),
                  Button(
                      child: const Text("Increase counter"), onPressed: () {}),
                  TextBox(
                    header: 'Enter your name:',
                    placeholder: 'Name',
                    expands: false,
                    highlightColor: Colors.orange.withOpacity(0.3),
                    unfocusedColor: Colors.red.withOpacity(0.9),
                  ),
                  Button(
                    child: const Text('Show dialog'),
                    onPressed: () => showContentDialog(context),
                  ),
                  // ListView.builder(
                  //     itemCount: contacts.length,
                  //     itemBuilder: (context, index) {
                  //       final contact = contacts[index];
                  //       return ListTile.selectable(
                  //         title: Text(contact),
                  //         selected: selectedContacts.contains(contact),
                  //         selectionMode: ListTileSelectionMode.multiple,
                  //         onSelectionChange: (selected) {
                  //           setState(() {
                  //             if (selected) {
                  //               selectedContacts.add(contact);
                  //             } else {
                  //               selectedContacts.remove(contact);
                  //             }
                  //           });
                  //         },
                  //       );
                  //     }),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  void showContentDialog(BuildContext context) async {
    final result = await showDialog<String>(
      context: context,
      builder: (context) => ContentDialog(
        title: const Text('Delete file permanently?'),
        content: Container(
          height: 150,
          child: Column(
            children: [
              ProgressRing(),
              const Text(
                'If you delete this file, you wont be able to recover it. Do you want to delete it?',
              ),
            ],
          ),
        ),
        actions: [
          Button(
            child: const Text('Delete'),
            onPressed: () {
              Navigator.pop(context, 'User deleted file');
              // Delete file here
            },
          ),
          FilledButton(
            child: const Text('Cancel'),
            onPressed: () => Navigator.pop(context, 'User canceled dialog'),
          ),
        ],
      ),
    );
    setState(() {});
  }
}
