import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:whatsapp_cw/whatsapp/model/model_config.dart';
import 'package:whatsapp_cw/whatsapp/utils/user_preference.dart';

class ApiUtilsRs {
  Future<Dio> dioRS(String contentType) async {
    UserPreferences _pre = UserPreferences();
    Map<String, dynamic> headerForm = {};

    BaseOptions optionsForm = BaseOptions(
      baseUrl: "https://licencia.rumisofperu.com/v1/",
      connectTimeout: 5000000 * 90,
      receiveTimeout: 5000000 * 90,
      contentType: contentType,
      headers: headerForm,
      validateStatus: (_) => true,
      
    );

    Dio dio = Dio(optionsForm);

    return dio;
  }
}
