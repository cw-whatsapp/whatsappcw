import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:whatsapp_cw/whatsapp/model/model_config.dart';
import 'package:whatsapp_cw/whatsapp/utils/user_preference.dart';

class ApiUtils {
  Future<Dio> dio(String contentType) async {
    UserPreferences _pre = UserPreferences();
    Map<String, dynamic> headerForm = {};
    String data = await rootBundle.loadString('assets/config.json');
    ModelConfig _model = ModelConfig();
    _model = ModelConfig.fromJson(json.decode(data));

    BaseOptions optionsForm = BaseOptions(
      baseUrl: _model.hostserver!,
      connectTimeout: 5000000 * 90,
      receiveTimeout: 5000000 * 90,
      contentType: contentType,
      headers: headerForm,
    );

    Dio dio = Dio(optionsForm);

    return dio;
  }
}
