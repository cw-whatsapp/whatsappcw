import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart' as d;
import 'package:whatsapp_cw/whatsapp/api/api.dart';
import 'package:whatsapp_cw/whatsapp/api/api_rs.dart';
import 'package:whatsapp_cw/whatsapp/model/model_auth.dart';
import 'package:whatsapp_cw/whatsapp/model/model_group.dart';
import 'package:whatsapp_cw/whatsapp/model/model_qr.dart';
import 'package:whatsapp_cw/whatsapp/model/model_response_send.dart';

import 'package:flutter/material.dart';
import 'package:whatsapp_cw/whatsapp/model/model_rs.dart';

class ApiWhatsapp {
  ApiUtils api = ApiUtils();

  Future<ModelRs> getRS() async {
    try {
      d.Response response;
      // ignore: no_leading_underscores_for_local_identifiers
      ApiUtilsRs _api = ApiUtilsRs();

      d.Dio dio = await _api.dioRS("application/json");

      var body = jsonEncode({"ruc": "10477931508", "password": "1233"});

      response = await dio.post("client/login", data: body);
      log("----------- licencia ${response.statusCode}");
      if (response.statusCode == 200 || response.statusCode == 401) {
        log(response.data.toString());
        return ModelRs.fromJson(response.data);
      }

      return ModelRs();
    } on d.DioError catch (e) {
      return ModelRs.error("-- error ${e.message}");
    }
  }

  Future<ModelAuth> getAuth() async {
    try {
      d.Response response;

      d.Dio dio = await api.dio("application/json");

      response = await dio.get(
        "group/checkauth",
      );

      debugPrint("GetAuth response json:$response");

      if (response.statusCode == 200) {
        return ModelAuth.fromJson(response.data);
      }
      return ModelAuth();
    } on d.DioError catch (e) {
      return ModelAuth.error(e.message);
    }
  }

  Future<ModelQr> getQR() async {
    try {
      d.Response response;

      d.Dio dio = await api.dio("application/json");

      response = await dio.get(
        "group/getqr",
      );

      debugPrint("GetQr response json:$response");

      if (response.statusCode == 200) {
        return ModelQr.fromJson(response.data);
      }
      return ModelQr();
    } on d.DioError catch (e) {
      return ModelQr.error(e.message);
    }
  }

  Future<ModelGroups> getGroup(String fill) async {
    try {
      d.Response response;

      d.Dio dio = await api.dio("application/json");

      response = await dio.post("group/getfiltersorlist", data: {
        'filtro': fill,
      });

      //debugPrint("GetGroup response json:$response");

      if (response.statusCode == 200) {
        return ModelGroups.fromJson(response.data);
      }
      return ModelGroups();
    } on d.DioError catch (e) {
      return ModelGroups.error(e.message);
    }
  }

  Future<ModelResponseSend> sendPdf(
      {required String id,
      required String pdfName,
      required String pdfName2}) async {
    try {
      d.Response response;

      d.Dio dio = await api.dio("application/json");

      response = await dio.post(
        "group/sendpdf",
        data: {"id": id, "pdfName": pdfName, "pdfName2": pdfName2},
      );

      debugPrint("GetGroup response json PDF:$response");

      if (response.statusCode == 200) {
        return ModelResponseSend.fromJson(response.data);
      }
      return ModelResponseSend();
    } on d.DioError catch (e) {
      return ModelResponseSend.error(e.message);
    }
  }

  Future<ModelResponseSend> sendPdfMini(String pdf, String name) async {
    try {
      d.Response response;

      d.Dio dio = await api.dio("application/json");

      response = await dio.post(
        "group/sendPdfMini",
        data: {"pdf": pdf, "docName": name},
      );

      debugPrint("GetGroup response json PDFMini:$response");

      if (response.statusCode == 200) {
        return ModelResponseSend.fromJson(response.data);
      }
      return ModelResponseSend();
    } on d.DioError catch (e) {
      return ModelResponseSend.error(e.message);
    }
  }

  Future<ModelResponseSend> sendImg({
    required String id,
    required String img,
  }) async {
    try {
      d.Response response;

      d.Dio dio = await api.dio("application/json");

      log("[$img]");
      response = await dio.post("group/sendimg", data: {"id": id, "img": img});

      debugPrint("GetGroup response json IMG:$response");

      if (response.statusCode == 200) {
        return ModelResponseSend.fromJson(response.data);
      }
      return ModelResponseSend();
    } on d.DioError catch (e) {
      return ModelResponseSend.error(e.message);
    }
  }

  Future<ModelResponseSend> sendMsj(String id, String msj) async {
    try {
      d.Response response;

      d.Dio dio = await api.dio("application/json");

      response = await dio
          .post("group/sendmessagegroup", data: {"id": id, "message": msj});

      debugPrint("GetGroup response json MSJ:$response");

      if (response.statusCode == 200) {
        return ModelResponseSend.fromJson(response.data);
      }
      return ModelResponseSend();
    } on d.DioError catch (e) {
      return ModelResponseSend.error(e.message);
    }
  }
}
