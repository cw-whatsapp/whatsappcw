//import 'package:rumisof_app/model/model_invoice/model_invoiceEXEL.dart';
//import 'package:rumisof_app/utils/UserPreferences.dart';
import 'dart:convert';

import 'package:syncfusion_flutter_xlsio/xlsio.dart' hide Column;
import 'package:whatsapp_cw/whatsapp/bloc/bloc_whatsapp/blocwhatsapp_bloc.dart';
import 'package:whatsapp_cw/whatsapp/model/mdoel_exel_retirados.dart';
import 'package:whatsapp_cw/whatsapp/model/model_exel_envio.dart';
import 'package:whatsapp_cw/whatsapp/model/model_group.dart';

//Local imports
import 'package:whatsapp_cw/whatsapp/utils/save_file_mobile.dart'
    if (dart.library.html) 'package:whatsapp_cw/whatsapp/utils/save_file_mobile.dart';

Future<void> generateExcelExistencia(
    List<ModelExelExistencia>? envios, GenerateExelExistencias event) async {
  final Workbook workbook = Workbook();

  final Worksheet sheet = workbook.worksheets[0];

  sheet.showGridlines = false;

  sheet.enableSheetCalculations();

  head(sheet, event);
  //factura
  int i = factura(sheet, envios, event);

  //boleta
  int j = i;
  //Footer
  j = j + 3;

  final Hyperlink hyperlink = sheet.hyperlinks.add(sheet.getRangeByName('A$j'),
      HyperlinkType.url, 'https://www.rumisofperu.com');
  hyperlink.screenTip = 'Generado por  Rumisof Perú tlf.929893850';
  hyperlink.textToDisplay = 'Generado por  Rumisof Perú tlf.929893850';

  final Range range10 = sheet.getRangeByName('A$j:M$j');
  range10.cellStyle.backColor = '#22BA8E';
  range10.cellStyle.fontColor = '#FFFFFF';
  range10.merge();
  range10.cellStyle.hAlign = HAlignType.center;
  range10.cellStyle.vAlign = VAlignType.center;

  //Save and launch the excel.
  final List<int> bytes = workbook.saveAsStream();
  //Dispose the document.
  workbook.dispose();

  //Save and launch the file.
  DateTime now = DateTime.now();

  print("${now.year}_${now.month}_${now.day}_${now.hour}_${now.minute}_${now.second}");

  String namFile = "${now.year}_${now.month}_${now.day}_${now.hour}_${now.minute}_${now.second}";

  await saveAndLaunchFile(bytes, '$namFile.xlsx');
}

void head(Worksheet sheet, GenerateExelExistencias event) {
  //sheet.pictures.addStream(2, 2, bytes5);

  sheet.getRangeByName('B9').setText("CW Perú Odontología");
  sheet.getRangeByName('B9').cellStyle.fontSize = 12;

  sheet.getRangeByName('B10').setText("F.E: ${event.envioDate.envioDate}");
  sheet.getRangeByName('B10').cellStyle.fontSize = 9;

  sheet.getRangeByName('B11').setText('TEL.: ${event.number}');
  sheet.getRangeByName('B11').cellStyle.fontSize = 9;

  final Range range1 = sheet.getRangeByName('F8:G8');
  final Range range2 = sheet.getRangeByName('F9:G9');
  final Range range3 = sheet.getRangeByName('F10:G10');
  final Range range4 = sheet.getRangeByName('F11:G11');
  final Range range5 = sheet.getRangeByName('F12:G12');

  range1.merge();
  range2.merge();
  range3.merge();
  range4.merge();
  range5.merge();

  sheet.getRangeByName('F10').setText('FECHA');
  range3.cellStyle.fontSize = 8;
  range3.cellStyle.bold = true;
  range3.cellStyle.hAlign = HAlignType.right;
  DateTime date = DateTime.now();
  sheet.getRangeByName('F11').dateTime = date;
  sheet.getRangeByName('F11').numberFormat =
      r'[$-x-sysdate]dddd, mmmm dd, yyyy';
  range4.cellStyle.fontSize = 9;
  range4.cellStyle.hAlign = HAlignType.right;

  range5.cellStyle.fontSize = 8;
  range5.cellStyle.bold = true;
  range5.cellStyle.hAlign = HAlignType.right;
}

int factura(Worksheet sheet, List<ModelExelExistencia>? envios,
    GenerateExelExistencias event) {
  int i = 14;

  envios!.forEach((element) {
    sheet.getRangeByIndex(i, 1).text =
        ' ${element.owner} - ${element.nameGroup}';
    sheet.getRangeByIndex(i, 1).cellStyle.fontSize = 8;

    final Range range9 = sheet.getRangeByName('A$i:M$i');
    range9.cellStyle.backColor = '#FF1744';
    range9.cellStyle.fontColor = '#FFFFFF';
    range9.merge();
    range9.cellStyle.hAlign = HAlignType.center;
    range9.cellStyle.vAlign = VAlignType.center;
    i = i + 1;

    sheet.getRangeByIndex(i, 2).setText('GRUPO');
    sheet.getRangeByIndex(i, 3).setText('NUMERO');
    sheet.getRangeByIndex(i, 4).setText('ESTADO');

    sheet.getRangeByName('B$i').columnWidth = 50;
    sheet.getRangeByName('C$i').columnWidth = 20;
    sheet.getRangeByName('D$i').columnWidth = 20;

    i = i + 1;

    element.participantt!.forEach((element6) {
      sheet.getRangeByIndex(i, 2).setText(element.nameGroup);
      sheet.getRangeByIndex(i, 3).setText("${element6.number}");
      sheet.getRangeByIndex(i, 4).setText("${element6.status}");
      i = i + 1;
    });

    i++;
  });

  return i;
}
