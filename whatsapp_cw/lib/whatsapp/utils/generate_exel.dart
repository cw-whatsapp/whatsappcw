//import 'package:rumisof_app/model/model_invoice/model_invoiceEXEL.dart';
//import 'package:rumisof_app/utils/UserPreferences.dart';
import 'dart:convert';
import 'dart:developer';

import 'package:syncfusion_flutter_xlsio/xlsio.dart' hide Column;
import 'package:whatsapp_cw/whatsapp/model/model_exel_envio.dart';
import 'package:whatsapp_cw/whatsapp/model/model_group.dart';

//Local imports
import 'package:whatsapp_cw/whatsapp/utils/save_file_mobile.dart'
    if (dart.library.html) 'package:whatsapp_cw/whatsapp/utils/save_file_mobile.dart';

Future<void> generateExcel(List<ModeExelEnvio>? envios) async {
  //Create a Excel document.
  //UserPreferences _pre = new UserPreferences();
  //Creating a workbook.
  final Workbook workbook = Workbook();
  //Accessing via index
  final Worksheet sheet = workbook.worksheets[0];

  sheet.showGridlines = false;

  // Enable calculation for worksheet.
  sheet.enableSheetCalculations();

  // Uint8List bytes5 = (await NetworkAssetBundle(Uri.parse(_pre.locationImage))
  //       .load(_pre.locationImage))
  //   .buffer
  //   .asUint8List();

  //head
  head(sheet);
  //factura
  int i = factura(sheet, envios);

  //boleta
  int j = i;
  //Footer
  j = j + 3;

  final Hyperlink hyperlink = sheet.hyperlinks.add(sheet.getRangeByName('A$j'),
      HyperlinkType.url, 'https://www.rumisofperu.com');
  hyperlink.screenTip = 'Generado por  Rumisof Perú tlf.929893850';
  hyperlink.textToDisplay = 'Generado por  Rumisof Perú tlf.929893850';

  final Range range10 = sheet.getRangeByName('A$j:M$j');
  range10.cellStyle.backColor = '#22BA8E';
  range10.cellStyle.fontColor = '#FFFFFF';
  range10.merge();
  range10.cellStyle.hAlign = HAlignType.center;
  range10.cellStyle.vAlign = VAlignType.center;

  //Save and launch the excel.
  final List<int> bytes = workbook.saveAsStream();
  //Dispose the document.
  workbook.dispose();

  //Save and launch the file.
  DateTime now = DateTime.now();

  print(
      "${now.year}_${now.month}_${now.day}_${now.hour}_${now.minute}_${now.second}");

  String namFile =
      "envio_${now.year}_${now.month}_${now.day}_${now.hour}_${now.minute}_${now.second}";

  // await saveAndLaunchFile(bytes, '$namFile.xlsx');

  await saveAndLaunchFile(bytes, '$namFile.xlsx');
}

void head(
  Worksheet sheet,
) {
  //sheet.pictures.addStream(2, 2, bytes5);

  sheet.getRangeByName('B9').setText("CW Perú Odontología");
  sheet.getRangeByName('B9').cellStyle.fontSize = 12;

  sheet.getRangeByName('B10').setText("contacto@cwperu.com.pe");
  sheet.getRangeByName('B10').cellStyle.fontSize = 9;

  sheet.getRangeByName('B11').setText('TEL.:+51 992268157');
  sheet.getRangeByName('B11').cellStyle.fontSize = 9;

  final Range range1 = sheet.getRangeByName('F8:G8');
  final Range range2 = sheet.getRangeByName('F9:G9');
  final Range range3 = sheet.getRangeByName('F10:G10');
  final Range range4 = sheet.getRangeByName('F11:G11');
  final Range range5 = sheet.getRangeByName('F12:G12');

  range1.merge();
  range2.merge();
  range3.merge();
  range4.merge();
  range5.merge();

  sheet.getRangeByName('F10').setText('FECHA');
  range3.cellStyle.fontSize = 8;
  range3.cellStyle.bold = true;
  range3.cellStyle.hAlign = HAlignType.right;
  DateTime date = DateTime.now();
  sheet.getRangeByName('F11').dateTime = date;
  sheet.getRangeByName('F11').numberFormat =
      r'[$-x-sysdate]dddd, mmmm dd, yyyy';
  range4.cellStyle.fontSize = 9;
  range4.cellStyle.hAlign = HAlignType.right;

  range5.cellStyle.fontSize = 8;
  range5.cellStyle.bold = true;
  range5.cellStyle.hAlign = HAlignType.right;
}

int factura(Worksheet sheet, List<ModeExelEnvio>? envios) {
  int i = 14;

  envios!.forEach((element) {
    sheet.getRangeByIndex(i, 1).text = '${element.envioDescription}';
    sheet.getRangeByIndex(i, 1).cellStyle.fontSize = 8;

    final Range range9 = sheet.getRangeByName('A$i:M$i');
    range9.cellStyle.backColor = '#FF1744';
    range9.cellStyle.fontColor = '#FFFFFF';
    range9.merge();
    range9.cellStyle.hAlign = HAlignType.center;
    range9.cellStyle.vAlign = VAlignType.center;
    i = i + 1;

    sheet.getRangeByIndex(i, 2).setText('GRUPO');
    sheet.getRangeByIndex(i, 3).setText('N° PARTICIPANTES');
    sheet.getRangeByIndex(i, 4).setText('NUMERO EMISOR');
    sheet.getRangeByIndex(i, 5).setText('USUARIO EMISOR');
    sheet.getRangeByIndex(i, 6).setText('FECHA');
    sheet.getRangeByIndex(i, 7).setText('DESCRIPCIÓN');
    sheet.getRangeByIndex(i, 8).setText('ESTADO');
    sheet.getRangeByName('B$i').columnWidth = 50;
    sheet.getRangeByName('C$i').columnWidth = 20;
    sheet.getRangeByName('D$i').columnWidth = 20;
    sheet.getRangeByName('E$i').columnWidth = 20;
    sheet.getRangeByName('F$i').columnWidth = 30;
    sheet.getRangeByName('G$i').columnWidth = 30;
    sheet.getRangeByName('H$i').columnWidth = 20;

    ModelGroups _model = ModelGroups();
    Map<String, dynamic> _map = Map<String, dynamic>();
    _map["data"] = element.envioJsonlOg!;

    log("${json.decode(_map["data"])}");

    _model = ModelGroups.fromJson(json.decode(_map["data"]));
    i = i + 1;
    _model.lista!.forEach((element2) {
      sheet.getRangeByIndex(i, 2).setText(element2.name);
      sheet.getRangeByIndex(i, 3).setText("${element2.participants!.length}");
      sheet.getRangeByIndex(i, 4).setText("${element.envioNumber}");
      sheet.getRangeByIndex(i, 5).setText(element.envioAdmin);
      sheet.getRangeByIndex(i, 6).setText("${element.envioDate}");
      sheet.getRangeByIndex(i, 7).setText("${element.envioDescription}");
      sheet
          .getRangeByIndex(i, 8)
          .setText("${(element.envioStatus == 1) ? '100%' : '0%'}");
      final Range range9 = sheet.getRangeByName('H$i:H$i');
      range9.cellStyle.backColor =
          (element.envioStatus == 1) ? '#17ff45' : '#FF1744';
      range9.cellStyle.fontColor = '#FFFFFF';
      range9.merge();
      range9.cellStyle.hAlign = HAlignType.center;
      range9.cellStyle.vAlign = VAlignType.center;

      i = i + 1;
    });

    print("---------data: ${_model.lista![0].name}");

    i++;
  });

  return i;
}
