import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  static final UserPreferences _instancia = new UserPreferences._internal();

  factory UserPreferences() {
    return _instancia;
  }

  UserPreferences._internal();

  late SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  String get id {
    return _prefs.getString('id') ?? '';
  }

  set id(String value) {
    _prefs.setString('id', value);
  }

  String get name {
    return _prefs.getString('name') ?? '';
  }

  set name(String value) {
    _prefs.setString('name', value);
  }

  String get password {
    return _prefs.getString('password') ?? '';
  }

  set password(String value) {
    _prefs.setString('password', value);
  }

  String get image {
    return _prefs.getString('image') ?? '';
  }

  set image(String value) {
    _prefs.setString('image', value);
  }

  String get roleId {
    return _prefs.getString('roleId') ?? '';
  }

  set roleId(value) {
    _prefs.setString('roleId', value);
  }

  String get roleName {
    return _prefs.getString('roleName') ?? '';
  }

  set roleName(value) {
    _prefs.setString('roleName', value);
  }

  String get rolePermissions {
    return _prefs.getString('rolePermissions') ?? '';
  }

  set rolePermissions(value) {
    _prefs.setString('rolePermissions', value);
  }

  /*host*/
  String get hostId {
    return _prefs.getString('hostId') ?? '';
  }

  set hostId(value) {
    _prefs.setString('hostId', value);
  }

  String get hostName {
    return _prefs.getString('hostName') ?? '';
  }

  set hostName(value) {
    _prefs.setString('hostName', value);
  }

  /*cash */
  String get cashId {
    return _prefs.getString('cashId') ?? '';
  }

  set cashId(value) {
    _prefs.setString('cashId', value);
  }

  /*  Location preferences */
  String get locationName {
    return _prefs.getString('locationName') ?? '';
  }

  set locationName(value) {
    _prefs.setString('locationName', value);
  }

  String get locationAddress {
    return _prefs.getString('locationAddress') ?? '';
  }

  set locationAddress(value) {
    _prefs.setString('locationAddress', value);
  }

  String get locationRuc {
    return _prefs.getString('locationRuc') ?? '';
  }

  set locationRuc(value) {
    _prefs.setString('locationRuc', value);
  }

  String get locationDepartment {
    return _prefs.getString('locationDepartment') ?? '';
  }

  set locationDepartment(value) {
    _prefs.setString('locationDepartment', value);
  }

  String get locationProvince {
    return _prefs.getString('locationDepartment') ?? '';
  }

  set locationProvince(value) {
    _prefs.setString('locationDepartment', value);
  }

  String get locationDistrict {
    return _prefs.getString('locationDistrict') ?? '';
  }

  set locationDistrict(value) {
    _prefs.setString('locationDistrict', value);
  }

  String get locationImage {
    return _prefs.getString('locationImage') ?? '';
  }

  set locationImage(value) {
    _prefs.setString('locationImage', value);
  }

  String get locationPass {
    return _prefs.getString('locationPass') ?? '';
  }

  set locationPass(value) {
    _prefs.setString('locationPass', value);
  }

  String get locationDB {
    return _prefs.getString('locationDB') ?? '';
  }

  set locationDB(value) {
    _prefs.setString('locationDB', value);
  }

  String get locationLicense {
    return _prefs.getString('locationLicense') ?? '';
  }

  set locationLicense(value) {
    _prefs.setString('locationLicense', value);
  }

  String get locationHeader1 {
    return _prefs.getString('locationHeader1') ?? '';
  }

  set locationHeader1(value) {
    _prefs.setString('locationHeader1', value ?? '');
  }

  String get locationHeader2 {
    return _prefs.getString('locationHeader2') ?? '';
  }

  set locationHeader2(value) {
    _prefs.setString('locationHeader2', value ?? '');
  }

  String get locationHeader3 {
    return _prefs.getString('locationHeader3') ?? '';
  }

  set locationHeader3(value) {
    _prefs.setString('locationHeader3', value ?? '');
  }

  String get locationFooter1 {
    return _prefs.getString('locationFooter1') ?? '';
  }

  set locationFooter1(value) {
    _prefs.setString('locationFooter1', value ?? '');
  }

  String get locationFooter2 {
    return _prefs.getString('locationFooter2') ?? '';
  }

  set locationFooter2(value) {
    _prefs.setString('locationFooter2', value ?? '');
  }

  String get idType {
    return _prefs.getString('idType') ?? '';
  }

  set idType(value) {
    _prefs.setString('idType', value ?? '');
  }

  String get printer1 {
    return _prefs.getString('printer1') ?? 'ZKP8008';
  }

  set printer1(value) {
    _prefs.setString('printer1', value ?? 'ZKP8008');
  }

//__________________________ WC__________________
  String get jsonEnvio {
    return _prefs.getString('jsonEnvio') ?? '';
  }

  set jsonEnvio(value) {
    _prefs.setString('jsonEnvio', value ?? '');
  }

  String get url {
    return _prefs.getString('url') ?? '';
  }

  set url(value) {
    _prefs.setString('url', value ?? '');
  }

  String get port {
    return _prefs.getString('port') ?? '';
  }

  set port(value) {
    _prefs.setString('port', value ?? '');
  }

  String get db {
    return _prefs.getString('db') ?? '';
  }

  set db(value) {
    _prefs.setString('db', value ?? '');
  }

  int get countTime {
    return _prefs.getInt('countTime') ?? 12;
  }

  set countTime(value) {
    _prefs.setInt('countTime', value ?? 12);
  }
}
