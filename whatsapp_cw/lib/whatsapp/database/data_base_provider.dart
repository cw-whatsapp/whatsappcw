import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:mysql1/mysql1.dart';
import 'package:whatsapp_cw/whatsapp/model/model_config.dart';

class DatabaseProvider {
  static final DatabaseProvider db = DatabaseProvider._();

  DatabaseProvider._();

  Future<MySqlConnection> get database async {
    MySqlConnection _database;

    _database = await initDB();

    return _database;
  }

  initDB() async {
    MySqlConnection conn;
    String data = await rootBundle.loadString('assets/config.json');
    ModelConfig _model = ModelConfig();
    _model = ModelConfig.fromJson(json.decode(data));
    var settings = await ConnectionSettings(
        host: _model.dbhost!,
        port: _model.dbport!,
        user: _model.dbuser,
        db: _model.db,
        password: _model.dbpassword);
    conn = await MySqlConnection.connect(settings);

    await conn.query('''
            CREATE TABLE IF NOT EXISTS t_envio (
                envio_id VARCHAR(100) PRIMARY KEY,
                envio_description TEXT,
                envio_jsonData TEXT,
                envio_jsonlOG TEXT,
                envio_number VARCHAR(20),
                envio_admin TEXT,
                envio_status INT DEFAULT 0,
                envio_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            )
            ''');
    return conn;
  }
}
